import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, BackHandler } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Menu, MenuProvider, MenuOptions, MenuOption, MenuTrigger } from "react-native-popup-menu";
import { renderers } from 'react-native-popup-menu';

const { SlideInMenu } = renderers;

export default class TaskScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: () => (
                <Menu renderer={SlideInMenu}
                    ref={navigation.getParam('saveMenuReference')}>
                    <MenuTrigger
                        customStyles={{
                            triggerOuterWrapper: {
                                marginRight: 5,
                                width: 40,
                                height: 40,
                                borderRadius: 40,
                                overflow: 'hidden',
                            },
                            triggerWrapper: {
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }
                        }}>
                        <MaterialIcons name="more-vert" size={30} color="black" />
                    </MenuTrigger>
                    <MenuOptions>
                        <MenuOption style={styles.menuOption}>
                            <MaterialIcons name="edit" size={25} color="gray" />
                            <Text style={styles.menuOptionTitle}>Edit</Text>
                        </MenuOption>
                        <MenuOption style={styles.menuOption}>
                            <MaterialIcons name="delete" size={25} color="gray" />
                            <Text style={styles.menuOptionTitle}>Delete</Text>
                        </MenuOption>
                    </MenuOptions>
                </Menu>
            ),
        };
    };

    saveMenuReference = (reference) => {
        this.menu = reference
    };

    componentDidMount() {
        this.props.navigation.setParams({ saveMenuReference: this.saveMenuReference });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        if (this.backHandler != undefined)
            this.backHandler.remove();
    }

    handleBackPress = () => {
        if (this.menu != undefined && this.menu._isOpen())
            this.menu.close()
        else
            this.props.navigation.goBack();
        return true;
    }

    render() {
        const task = this.props.navigation.state.params.task;
        return (
            <View style={styles.container}>
                <Text style={styles.title}>{task.title}</Text>
                <Text style={styles.description}>{task.description}</Text>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F6F6',
    },
    title: {
        fontSize: 45
    },
    description: {
        fontSize: 20
    },
    menuOption: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    menuOptionTitle: {
        fontSize: 16,
        marginLeft: 8,
        color: 'gray'
    }
});