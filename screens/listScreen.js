import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, Platform, NativeModules } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import TaskEntry from '../components/TaskEntry.js';
import { Header } from 'react-navigation-stack';
import * as Localization from 'expo-localization';
import moment from "moment";
import "moment/min/locales.min";
import { TouchableNativeFeedback } from 'react-native-gesture-handler';

export default class ListScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        /*const deviceLanguage = Localization.locale;
        moment.locale(deviceLanguage);
        var day = moment().format('dddd Do');
        var month = moment().format('MMMM');
        day = day.charAt(0).toUpperCase() + day.slice(1);
        month = month.charAt(0).toUpperCase() + month.slice(1);*/
        return {
            headerLeft: () => (
                <View style={styles.burgerToggle}>
                    <TouchableNativeFeedback style={{ padding: 5 }}onPress={() => { navigation.toggleDrawer() }}>
                        <MaterialIcons name="menu" size={30} color="black" />
                    </TouchableNativeFeedback>
                </View>
            ),
            title: 'Tasks',
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={Tasks}
                    renderItem={({ item }) => (
                        <TaskEntry
                            style={styles.task} pressEvent={() => {
                                //alert(item.title);
                                this.props.navigation.navigate(
                                    'Task',
                                    { task: item }
                                );
                            }}
                            title={item.title} />
                    )}
                    ItemSeparatorComponent={() => (
                        <View style={styles.separator} />
                    )}
                    keyExtractor={item => item.id.toString()}
                />
                <TouchableOpacity onPress={() => alert('FAB clicked')} style={styles.fab}>
                    <MaterialIcons name="add" size={40} color="white" />
                </TouchableOpacity>
            </View>
        );
    }
};

//hardcoded tasks list
const Tasks = [
    {
        id: 0,
        title: 'Task1',
        description: 'This is task 1'
    },
    {
        id: 1,
        title: 'Task2',
        description: 'This is task 2'
    },
    {
        id: 2,
        title: 'Task3',
        description: 'This is task 3'
    },
    {
        id: 3,
        title: 'Task4',
        description: 'This is task 4'
    }
];

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F6F6',
    },
    fab: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 20,
        backgroundColor: '#03A9F4',
        borderRadius: 30,
        elevation: 8
    },
    task: {
        alignSelf: 'stretch',
        height: 60,
        fontSize: 20,
        backgroundColor: 'white',
        color: 'black',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    separator: {
        backgroundColor: 'rgb(200, 199, 204)',
        height: StyleSheet.hairlineWidth,
        alignSelf: 'stretch'
    },
    headerTitleWrapper: {
        flex: 2,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center'
    },
    headerfakeTitleWrapper: {
        flex: 1,
        justifyContent: 'center'
    },
    headerTitle: {
        fontWeight: 'bold',
        fontSize: 25
    },
    burgerToggle: {
        marginLeft: 8,
        borderRadius: 40,
        overflow: 'hidden'
    }
});