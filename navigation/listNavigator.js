import { createStackNavigator } from 'react-navigation-stack';
import ListScreen from '../screens/listScreen';
import TasklScreen from '../screens/taskScreen';

export default createStackNavigator(
    {
        List: ListScreen,
        Task: TasklScreen
    },
    {
        initialRouteName: 'List'
    }
)
