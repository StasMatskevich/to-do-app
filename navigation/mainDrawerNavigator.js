import { createDrawerNavigator } from 'react-navigation-drawer';
import ListNavigator from './listNavigator'

export default createDrawerNavigator(
    {
        ToDoList: ListNavigator,
        //Task: TasklScreen
    },
    {
        initialRouteName: 'ToDoList'
    }
)