import React, { Component } from 'react';
import { Animated, StyleSheet, Text, View, TouchableOpacity, Vibration } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import Swipeable from 'react-native-gesture-handler/Swipeable';

export default class TaskEntry extends Component {
    renderRightActions = (progress, dragX) => {
        var value = dragX.interpolate({
            inputRange: [-100, -65, 0],
            outputRange: [1, 0, 0],
            extrapolate: 'clamp'
        });
        return (
            <View style={[styles.swipeContainer, { justifyContent: 'flex-end' }]}>
                <Animated.View style={[styles.swipeOverflow, { backgroundColor: 'red', opacity: value }]}></Animated.View>
                <Animated.Text style={[styles.swipeActionLabel, { opacity: value }]}>Delete</Animated.Text>
                <MaterialIcons style={{marginRight: 8}} name="delete" size={40} color="white" />
            </View>
        );
    };

    renderLeftActions = (progress, dragX) => {
        var value = dragX.interpolate({
            inputRange: [0, 65, 100],
            outputRange: [0, 0, 1],
            extrapolate: 'clamp'
        });
        return (
            <View style={[{ justifyContent: 'flex-start' }, styles.swipeContainer]}>
                <Animated.View style={[styles.swipeOverflow, { backgroundColor: 'green', opacity: value }]}></Animated.View>
                <MaterialIcons style={{marginLeft: 8}} name="done" size={40} color="white" />
                <Animated.Text style={[styles.swipeActionLabel, { opacity: value }]}>Complete</Animated.Text>
            </View>
        );
    };

    onOpen = () => {
        this.swipeableReference.close();
    }

    updateReference = (reference) => {
        this.swipeableReference = reference;
    };

    render() {
        const { style, title, pressEvent } = this.props;
        return (
            <Swipeable
                ref={this.updateReference}
                containerStyle={style}
                childrenContainerStyle={[style, { backgroundColor: 'lightgray' }]}
                rightThreshold={95}
                leftThreshold={120}
                renderRightActions={this.renderRightActions}
                renderLeftActions={this.renderLeftActions}
                onSwipeableOpen={this.onOpen}
                onSwipeableWillOpen={() => { Vibration.vibrate(500); }}>
                <TouchableOpacity style={style} activeOpacity={0.75} onPress={pressEvent}>
                    <Text style={[styles.title, { fontSize: style.fontSize }]}>{title}</Text>
                </TouchableOpacity>
            </Swipeable>
        );
    }
};

const styles = StyleSheet.create({
    title: {
        marginLeft: 5
    },
    swipeContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'lightgray'
    },
    swipeActionLabel: {
        color: 'white',
        fontSize: 16
    },
    swipeOverflow: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    }
});  