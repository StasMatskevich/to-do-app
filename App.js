import React from 'react';
import { SafeAreaView } from 'react-native';
import { createAppContainer } from 'react-navigation';
import DrawerNavigator from './navigation/mainDrawerNavigator'
import { MenuProvider } from 'react-native-popup-menu';

const AppContainer = createAppContainer(DrawerNavigator);

export default function App() {
  return (
    <MenuProvider>
      <AppContainer />
    </MenuProvider>
  );
}
